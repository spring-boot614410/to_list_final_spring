package com.example.to_do_list_final_project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ToDoListFinalProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(ToDoListFinalProjectApplication.class, args);
	}

}
